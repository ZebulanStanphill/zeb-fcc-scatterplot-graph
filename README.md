# Zeb’s freeCodeCamp Scatterplot Graph

## Description
A scatterplot graph made using D3.js for a freeCodeCamp project.

## Usage
1. Open a terminal at the project root.
2. Run `npm install` to install the package dependencies.
3. Run `npm run build` to build the project.
4. Open `dist/index.html` to view/use the project.

## Licensing info

© 2019 Zebulan Stanphill

This program is free (as in freedom) software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

This project loads the testable-projects-fcc script:

https://github.com/freeCodeCamp/testable-projects-fcc

© 2017 freeCodeCamp

Released under the BSD 3-Clause License:

https://github.com/freeCodeCamp/testable-projects-fcc/blob/master/LICENSE.md