// External dependencies
import { max, min } from 'd3-array';
import { axisBottom, axisLeft } from 'd3-axis';
import { scaleTime } from 'd3-scale';
import { select } from 'd3-selection';
import { timeSecond, timeYear } from 'd3-time';
import { timeFormat, timeParse } from 'd3-time-format';

// Project asset dependencies
import data from './assets/data.json';

const height = 500;
const width = 1000;
const padding = 20;
const leftPadding = 50;
const rightPadding = 200;
const bottomPadding = 60;
const rangeYMin = padding;
const rangeYMax = height - bottomPadding;
const rangeXMin = leftPadding;
const rangeXMax = width - rightPadding;

const parseMMSS = timeParse( '%M:%S' );
const parseYear = timeParse( '%Y' );

const xScale = scaleTime()
	.domain( [
		min( data, d => parseYear( d['Year'] ) ),
		max( data, d => parseYear( d['Year'] ) )
	] )
	.range( [ rangeXMin, rangeXMax ] );

const yScale = scaleTime()
	.domain( [
		min( data, d => parseMMSS( d['Time'] ) ),
		max( data, d => parseMMSS( d['Time'] ) )
	] )
	.range( [ rangeYMin, rangeYMax ] );

// Append svg to root element.
const svg = select( '#app-root' ).append( 'svg' )
	.attr( 'class', 'scatterplot-graph' )
	.attr( 'viewBox', `0 0 ${ width } ${ height }` );

// Append title to svg.
svg.append( 'text' )
	.attr( 'id', 'title' )
	.attr( 'x', width / 2 )
	.attr( 'y', 30 )
	.attr( 'font-size', 24 )
	.text( 'Doping in Professional Bicycle Racing' );

const yAxis = axisLeft( yScale )
	.tickFormat( timeFormat( '%M:%S' ) )
	.ticks( timeSecond.every( 15 ) );

// Append y-axis to svg.
svg.append( 'g' )
	.attr( 'id', 'y-axis' )
	.attr( 'transform', `translate(${ rangeXMin - 5 }, 0)` )
	.call( yAxis );

const xAxis = axisBottom( xScale )
	.ticks( timeYear.every( 1 ) );

// Append x-axis to svg.
svg.append( 'g' )
	.attr( 'id', 'x-axis' )
	.attr( 'transform', `translate(0, ${ rangeYMax + 5 })` )
	.call( xAxis );

svg.selectAll( '.dot' )
	.data( data )
	.enter()
	.append( 'circle' )
	.attr(
		'class',
		d => d['Doping'] === '' ?
			'dot dot--no-doping' :
			'dot dot--doping'
	)
	.attr( 'data-xvalue', d => parseYear( d['Year'] ) )
	.attr( 'data-yvalue', d => parseMMSS( d['Time'] ) )
	.attr( 'data-rider', d => d['Name'] )
	.attr( 'data-doping', d => d['Doping'] )
	.attr( 'cx', d => xScale( parseYear( d['Year'] ) ) )
	.attr( 'cy', d => yScale( parseMMSS( d['Time'] ) ) ) 
	.attr( 'r', 3 )
	.on( 'mouseenter', function() {
		const dotX = this.getAttribute( 'cx' );
		const dotY = this.getAttribute( 'cy' );
		const rider = this.getAttribute( 'data-rider' );
		const date = this.getAttribute( 'data-xvalue' );
		const time = this.getAttribute( 'data-yvalue' );
		const dopingAllegations = this.getAttribute( 'data-doping' );

		const formatMMSS = timeFormat( '%M:%S' );

		select( '#tooltip' )
			.style( 'display', 'block' )
			.attr( 'data-year', date )
			.attr( 'transform', `translate(${ dotX }, ${ dotY - 24 })` );

		select( '#tooltip-text-1' )
			.text(
				`${ rider }, ` +
				`${ new Date( date ).getFullYear() }, ` +
				`${ formatMMSS( new Date( time ) ) }`
			);

		select( '#tooltip-text-2' )
			.text(
				dopingAllegations !== '' ? `${ dopingAllegations }` : ''
			);
	} )
	.on( 'mouseleave', function() {
		select( '#tooltip' )
			.style( 'display', 'none' );
	} );

const legend = svg.append( 'g' )
	.attr( 'id', 'legend' )
	.attr( 'transform', `translate(${ ( width / 2 ) - 104 }, ${ 45 })` );

legend.append( 'rect' )
	.attr( 'width', 208 )
	.attr( 'height', 42 )
	.style( 'fill', '#111' );

legend.append( 'circle' )
	.attr( 'class', 'dot--doping' )
	.attr( 'cx', 8 )
	.attr( 'cy', 12 )
	.attr( 'r', 3 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 16 )
	.attr( 'y', 12 )
	.text( 'rider with doping allegations' );

legend.append( 'circle' )
	.attr( 'class', 'dot--no-doping' )
	.attr( 'cx', 8 )
	.attr( 'cy', 30 ) 
	.attr( 'r', 3 );

legend.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 16 )
	.attr( 'y', 30 )
	.text( 'rider without doping allegations' );

const tooltip = svg.append( 'g' )
	.attr( 'id', 'tooltip' );

tooltip.append( 'text' )
	.attr( 'id', 'tooltip-text-1' );

tooltip.append( 'text' )
	.attr( 'id', 'tooltip-text-2' )
	.attr( 'y', 16 );